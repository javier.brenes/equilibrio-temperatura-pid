//CODIGO PROYECTO CONTROL DE TEMPERATURA

//LIBRERIAS
#include <EasyButton.h>
#include <TimerOne.h>
#include <SimpleTimer.h>
#include <EEPROM.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
//#include <LiquidCrystal.h>

//PINES ENTRADA/SALIDA

#define sensorTemperatura A0
#define ventilador 10
#define botonSELECT 6
#define botonUP 7
#define botonDOWN 8

//VARIABLES
float targetTemp = 30.5;   // Temperatura objetivo deseada para nuestro sistema
float softTemp = 0.4;      // Factor temperatura 0<softTemp<=1 (1 no soft) sirve para coger mas peso el valor de un lado o el de otro

//PARAMETROS PARA ALGORITMO PID (Y PARA ALGORITMO PROPORCIONAL)
float kp = 12;            // Parámetro proporcional % / C    (10 - 40)
float ki = 0.2;           // Parámetro Integral %/(C * s)    (0.1 - 2)
float kd = 2;             // Parámetro Derivado % * s / C  (0 - 30)
float error;              // temperatura actual - temperatura obj

float dutyInt = 0;        // Componente integral
float dutyProp;           // Componente proporcional
float dutyDer;            // Componente derivada

float duty = 0;             // porcentaje al que se encuentra el ventilador girando
float temp;                 // Temperatura filtrada
float tempRaw;              // Temperatura directa del sensor

//Valores guardados de la iteracion previa
float dutyPID0;
float temp0;
float targetTemp0;

//Valores para la velocidad del ventilador
unsigned int ventiladorOff = 0;  // Ventilador off si 1 - duty
unsigned int nVentiladores = 1;  // Número de ventiladores del sistema

//Tiempos de ejecucion
const int tiempoEjecucionMenu = 500;
const int cycle = 40;
const int tiempoEjecucionAlgoritmos = 5000;

//LIMITES
//Limite de las varibales proporcional, integral y derivativa para los algoritmos
float propMax = 100.0;    
float intMax = 100.0;     
float derMax = 100.0;     

//Velocidad ventilador
float velMin = 0.0;        // Mínimo duty cycle
float velMax = 100.0;      // Máximo duty cycle 

//CREACIÓN DE BOTONES
EasyButton buttonSELECT(botonSELECT);
EasyButton buttonUP(botonUP);
EasyButton buttonDOWN(botonDOWN);

// Contadores de botones para el menu del sistema en el LCD
int contbot1 = 0;
int contbot2 = 0;
int contbot3 = 0;

//Para saber si entrar en el menu serial
boolean menuSerial = false;
float tempIntroducida = -1;

// CREACION DEL OBJETO PARA EL DISPLAY LCD
LiquidCrystal_I2C lcd(0x38,16,2);
//LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// Inicializa el objeto timer de la libreria SimpleTimer
SimpleTimer timerPid;          //timer para la ejecucion de los algoritmos
SimpleTimer timerMenu;         //timer para la ejecucion del menu para la pantalla LCD

int timerIDPID = 0;          //Para parar el timer del algoritmo PID en el menu
int timerIDOnOff = 0;        //Para parar el timer del algoritmo ON/OFF en el menu
int timerIDProporcional = 0; //Para parar el timer del algoritmo proporcional en el menu

//******************************************************************************
//Funcion para calcular temperatura
float getTempLM35(int analogPin)
{
  int value = analogRead(analogPin);
  float millivolts = value * 1100.0 / 1023.0 ;
  float celsius = millivolts / 10.0;
  return celsius;
}

//Funciones para cargar y guardar en memoria interna de Arduino la temperatura
void eepromLoad()
{
  // Target temperature
  int val = EEPROM.read(0);
  if (val >= 10 && val <= 70) {
    targetTemp = float(val);
    targetTemp0 = targetTemp;
  }
}

void eepromSave()
{
  EEPROM.write(0, int(targetTemp));
  Serial.println("Valores de temperatura guardados en EEPROM.");
}

//Funciones para cargar y guardar en memoria interna de Arduino el estado anterior del algoritmo al reinicio

void eepromLoadEstado()
{
  // Estado algoritmo
  contbot1 = EEPROM.read(1);
  contbot2 = EEPROM.read(2);
  contbot3 = EEPROM.read(3);
}

void eepromSaveEstado()
{
  EEPROM.write(1, int(contbot1));
  EEPROM.write(2, int(contbot2));
  EEPROM.write(3, int(contbot3));
  Serial.println(F("Valores de estado algoritmo guardado en EEPROM."));
}

//Funcion para mostrar informacion mediante serial
void mostrarDatosSerial() {
  Serial.print(F("Temp sensor |"));
  Serial.print(F("  Temp suav   |"));
  Serial.print(F(" Temp deseada |"));
  Serial.print(F("  Vel en ptje  |"));
  Serial.print(F("  Velocidad    |"));
  Serial.print('\n');

  Serial.print(tempRaw);
  Serial.print(F("          "));
  Serial.print(temp);
  Serial.print(F("          "));
  Serial.print(targetTemp);
  Serial.print(F("          "));
  Serial.print(duty);                     //VELOCIDAD EN PORCENTAJE
  Serial.print(F("          "));
  Serial.print(duty / 100.0 * 1023.0);    //VELOCIDAD
  Serial.print(F("      "));
  Serial.print(F("          "));
  Serial.print('\n');
}

//Funcion para mostrar temperaturas en LCD
void mostratTempEnLCD(){
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("TEMP ACT  ");
      float tempact = getTempLM35(sensorTemperatura);
      lcd.print(tempact);
      lcd.setCursor(0, 1);
      lcd.print("TEMP OBJ  ");
      lcd.print(targetTemp);
}

//ALGORITMO DE CONTROL
void controlTemperaturaPID() {
  tempRaw = getTempLM35(sensorTemperatura);           //Lectura temperatura del sensor
  temp = tempRaw * softTemp + temp0 * (1 - softTemp); //Proceso para obtener la temperatura media filtrada
  error = tempRaw - targetTemp;                       //Error = temperatura leida del sensor - temperatura deseada

  if(contbot1==1){    //ALGORITMO PID

  //Calculo proporcional
  //dutyProp = kp *  error;
  //Usamos contrain para poner los limites
  dutyProp = constrain(kp * error, -propMax, propMax);
  //Calcula integral
  dutyInt = dutyInt + ki * (tiempoEjecucionAlgoritmos / 1000.0) * error;
  //Usamos contrain para poner los limites
  dutyInt = constrain(dutyInt, 0.0, intMax);
  //Calcula derivada
  dutyDer = kd * (temp - temp0) / tiempoEjecucionAlgoritmos * 1000;
  //Usamos contrain para poner los limites
  dutyDer = constrain(dutyDer, -derMax, derMax);
  // Suma los calculos
  duty = dutyProp + dutyInt + dutyDer;

  } else if(contbot1==2){              //PARA ALGORITMO ON/OFF
   
    if (temp > targetTemp) {
         duty = 100;  
  } else {
         duty = 0;
  } 
  
  } else if(contbot1==3){               //PARA ALGORITMO PROPORCIONAL
    dutyProp = constrain(kp * error, -propMax, propMax);
   // Duty limits
    duty = constrain(dutyProp, velMin, velMax);
  }

  // Duty limits
  duty = constrain(duty, velMin, velMax);
  
//Examinamos el número de ventiladores
if(nVentiladores == 1){
   if (ventiladorOff == 1 && duty > 10) { //mínimo para forzar el funcionamiento del ventilador
    duty = 17;                          //mínimo para arrancar el ventilador desde stop
    ventiladorOff = 0;
  } 
  // Indica si el ventilador esta off
  if (duty < 8)  {                       //Mínimo que hace que el ventilador deje de moverse
    ventiladorOff = 1;
  }
 
} else{ //Para mas de un ventilador
    if (ventiladorOff == 1 && duty > 8) {
    duty = 60;
    ventiladorOff = 0;
  }

  // Indica si el ventilador esta off
  if (duty < 15)
    ventiladorOff = 1;
}

  // Guarda los valores de las variables para la siguiente iteracion
  dutyPID0 = duty;
  temp0 = temp;
  targetTemp0 = targetTemp;

  Timer1.setPwmDuty(ventilador, duty / 100.0 * 1023.0);

  // Mostrar los datos en monitor serial
   mostrarDatosSerial();
} //HASTA AQUI ALGORITMO DE CONTROL

void menu() {
  if (buttonSELECT.read()) contbot1 = contbot1 + 1; //Con esta opcion, cambiaremos las distintas opciones disponibles a realizar
  if (buttonUP.read()) contbot2 = contbot2 + 1;     //Con esta opcion, activaremos el algoritmo PID y Proporcional cuando nos encontremos en el submenu ALGORITMO PID o ALG PROPORCIONAL
  if (buttonDOWN.read()) contbot3 = contbot3 + 1;   //Con esta opcion, activaremos el algoritmo ON/OFF cuando nos encontremos en el submenu ALGORITMO ON/OFF

  switch (contbot1) {  //Mediante el boton de select iremos cambiando de submenu segun la opcion deseada
    //Caso numero 1: Ejecucion del algoritmo PID
    case 1:
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("ALGORITMO PID");
      
      //Si pulsamos boton UP ejecutamos el algoritmo PID
      if (contbot2 == 1) {
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("EN EJECUCION");

        Serial.print(F("------------ALGORITMO PID EN EJECUCION --------------------------"));
        Serial.print('\n');

        delay(1000);
        eepromSaveEstado(); //Guardamos el estado por si se reinicia el controlador
        timerIDPID = timerPid.setInterval(tiempoEjecucionAlgoritmos, controlTemperaturaPID);
        contbot2 = 2;            //Cambiamos el valor de la variable para no ejecutar el algoritmo x veces
      }
      lcd.setCursor(0, 1);
      lcd.print("Vel: ");
      lcd.print(duty);
      lcd.print("%");
      
      if (buttonUP.pressedFor(3000)) {  //Si dejamos el boton up pulsado durante 3 segundos, muestra los valores kd,ki,kp del algoritmo PID
       
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("kp    ki   kd");
        lcd.setCursor(0, 1);
        lcd.print(kp);
        lcd.print(" ");
        lcd.print(ki);
        lcd.print(" ");
        lcd.print(kd);
      }

      if (buttonDOWN.pressedFor(3000)) {  //Si dejamos el boton down pulsado durante 3 segundos, muestra los valores de las temperatura actual y objetivo
          mostratTempEnLCD();
      }
      
      contbot3 = 0; // lo ponemos a 0 para poder entrar en el algoritmo siguiente
      break;
      
    //Caso numero 2: Ejecucion del algoritmo ON/OFF
    case 2:
      contbot2 = 0;              //Para poder ejecutar el algoritmo proprcional
      timerPid.disable(timerIDPID); //Desactivamos el control PID del caso anterior
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("ALGORITMO ON/OFF");
       

      //Si pulsamos boton DOWN ejecutamos el algoritmo ON/OFF
      if (contbot3 == 1) {
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("EN EJECUCION");
        Serial.print(F("------------ALGORITMO ON/OFF EN EJECUCION ------------------------------"));
        Serial.print('\n');
        delay(1000);
        eepromSaveEstado(); //Guardamos el estado por si se reinicia el controlador
        timerIDOnOff = timerPid.setInterval(tiempoEjecucionAlgoritmos, controlTemperaturaPID);
        contbot3 = 2;           //Cambiamos el valor de la variable para no ejecutar el algoritmo x veces
      }
      lcd.setCursor(0, 1);
      lcd.print("Vel: ");
      lcd.print(duty);
      lcd.print("%");

      if (buttonDOWN.pressedFor(3000)) {
          mostratTempEnLCD();
      }
      break;

    //Caso numero 3: Ejecucion del algoritmo Proporcional
    case 3:

     timerPid.disable(timerIDOnOff); //Desactivamos el control ON/OFF del caso anterior

      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("ALG PROPORCIONAL");

      if (contbot2 == 1) {
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("EN EJECUCION");
          Serial.print(F("------------ALGORITMO PROPORCIONAL EN EJECUCION ----------------------------"));
          Serial.print('\n');
        eepromSaveEstado(); //Guardamos el estado por si se reinicia el controlador
        delay(1000);
        timerIDProporcional = timerPid.setInterval(tiempoEjecucionAlgoritmos, controlTemperaturaPID);
        contbot2 = 2;            //Cambiamos el valor de la variable para no ejecutar el algoritmo x veces
      }
      lcd.setCursor(0, 1);
      lcd.print("Vel: ");
      lcd.print(duty);
      lcd.print("%");

      if (buttonDOWN.pressedFor(3000)) {
          mostratTempEnLCD();
      }

      
      break;

    //Caso numero 4: Configuracion de la temperatura(valores enteros)
    case 4:

      timerPid.disable(timerIDProporcional); //Desactivamos el control Proporcional del caso anterior
      duty = 0;
      Timer1.setPwmDuty(ventilador, duty);   //Paramos el ventilador ya que ningun algoritmo se esta ejecutando
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("CONFIG TEMP INT");
      
      if (buttonUP.isPressed() && targetTemp < 100) {
        targetTemp0 = targetTemp;
        targetTemp = targetTemp + 1;
      }
      if (buttonDOWN.isPressed() && targetTemp > 10) {
        targetTemp0 = targetTemp;
        targetTemp = targetTemp - 1;
      }

      lcd.setCursor(0, 1);
      lcd.print("TEMP OBJ ");
      lcd.print(targetTemp);
      break;

    //Caso numero 5: Configuracion de la temperatura(valores decimales)
    case 5:
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("CONFIG TEMP DEC");

      if (buttonUP.isPressed() && targetTemp < 100) {
        targetTemp0 = targetTemp;
        targetTemp = targetTemp + 0.1;
      }
      if (buttonDOWN.isPressed() && targetTemp > 10) {
        targetTemp0 = targetTemp;
        targetTemp = targetTemp - 0.1;
      }

      lcd.setCursor(0, 1);
      lcd.print("TEMP OBJ ");
      lcd.print(targetTemp);
      break;

    //Caso numero 6: Guarda la temperatura objetivo actual en la EEPROM
    case 6:
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("PULSE 2 SEG UP");
      lcd.setCursor(0, 1);
      lcd.print("Y GUARDAR TEMP");

      //Guarda en la memoria de arduino la temperatura actual si presionamos el boton UP durante dos segundos
      if (buttonUP.pressedFor(2000)) {
        eepromSave();
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("TEMP SAVE OK");
        lcd.setCursor(0, 1);
        lcd.print(targetTemp);
      }

      break;

    //Caso numero 7: Menu Serial
    case 7:
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("INSERTAR TEMP");
      lcd.setCursor(0, 1);
      lcd.print("POR PTO. SERIE");

      //Activamos la variable menuSerial, para la cual dentro del bucle principal loop, nos habilita introducir la temperatura por el monitor serie.
      menuSerial = true;

      break;


    //Caso numero 8: Configuracion de fabrica
    case 8:
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("CONFIG FABRICA");
      lcd.setCursor(0, 1);
      lcd.print("PULSE UP 5 SEG");
      menuSerial = false;
      if (buttonUP.pressedFor(5000)) {
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("RESTAURADO");
        lcd.setCursor(0, 1);
        lcd.print("FABRICA");
        targetTemp = 25;
        kp = 15;
        ki = 0.2;
        kd = 2;
      } break;

    //Caso numero 9: Modificar los valores de las constantes kp,ki,kd
    case 9:
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("CONFIG VAL PID");
      lcd.setCursor(0, 1);
      lcd.print("PULSE UP 5 SEG");
      if (buttonUP.pressedFor(5000)) {
        contbot1=997;
      }
    
      break;

      //Casos para cambiar los valores del PID
      case 997: //Caso para cambiar valor de kp
      
      if (buttonUP.isPressed() && kp < 40) {
         kp++;
      }
      if (buttonDOWN.isPressed() && kp > 5) {
        kp--;
      }

      lcd.clear();
      lcd.setCursor(0, 1);
      lcd.print("kp --> ");
      lcd.print(kp);
       break;

      case 998: //Caso para cambiar valor de ki
      
      if (buttonUP.isPressed() && ki < 2) {
         ki=ki+0.1;
      }
      if (buttonDOWN.isPressed() && ki > 0.1) {
         ki=ki-0.1;
      }

      lcd.clear();
      lcd.setCursor(0, 1);
      lcd.print("ki --> ");
      lcd.print(ki);
      break;

      case 999: //Caso para cambiar valor de kd
      
      if (buttonUP.isPressed() && kd < 40) {
         kd++;
      }
      if (buttonDOWN.isPressed() && kd > 0) {
         kd--;
      }

      lcd.clear();
      lcd.setCursor(0, 1);
      lcd.print("kd --> ");
      lcd.print(kd);
       break;
      
    //Caso defecto: Solo muestra informacion
    default:
      //Ponemos los contadores del valor de los botones a 0
      contbot1 = 0;
      contbot2 = 0;
      contbot3 = 0 ;
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("TEMP ACT  ");
      float tempact = getTempLM35(sensorTemperatura);
      lcd.print(tempact);
      lcd.setCursor(0, 1);
      lcd.print("TEMP OBJ  ");
      lcd.print(targetTemp);
      menuSerial = false;
      // eepromSaveEstado(); //Guardamos el estado por si se reinicia el controlador
      break;
  }
}

//---------------------------------------------------------------------------------------------------------

void setup() {

  Serial.begin(9600);
  Serial.setTimeout(50);
  Serial.println("Pulse el boton Select para ejecutar las opciones disponibles ...");
  Serial.println('\n');
  
  // Carga desde la EEPROM los valores almacenados para la temperatura
  eepromLoad();
  // Carga desde la EEPROM los valores almacenados por si se produce reinicio inesperado, volver al estado anterior
  eepromLoadEstado();

  // Inicializa LCD
  lcd.init();
  lcd.backlight();
  //lcd.begin(16, 2); //Para LCD 16x2
  
  lcd.print("Bienvenido");
  delay(2000);
  lcd.clear();
  lcd.print("Cargando el");
  lcd.setCursor(0, 1);
  lcd.print("estado anterior");
  delay(1500);

  //Inicializacion de los objetos de los botones
  buttonSELECT.begin();
  buttonUP.begin();
  buttonDOWN.begin();

  // Set analog reference to internal 1.1V
  analogReference(INTERNAL);

   //Ponemos el pin del ventilador como salida de Arduino y lo inicializamos para ver que la conexion esta bien.
  pinMode(ventilador, OUTPUT);
  digitalWrite(ventilador, HIGH);
  delay(2000);

  // Inicializamos el pwm y lo ejecutamos cada cycle
  Timer1.initialize(cycle);
  Timer1.pwm(ventilador, duty / 100.0 * 1023.0);

  // Control setup - temperatura inicial
  temp0 = getTempLM35(sensorTemperatura);
  temp = temp0;

  //Ejecutamos el menu cada tiempo determinado
  timerMenu.setInterval(tiempoEjecucionMenu, menu);
}

void loop() {

  //Lectura continua de los botones
  buttonSELECT.read();
  buttonUP.read();
  buttonDOWN.read();

  //Activamos los timers que tengamos creados
  timerPid.run();
  timerMenu.run();

  //Condicion para introducir la temperatura por el monitor serial
  if (menuSerial) {

    if (Serial.available() > 0 ) {
      tempIntroducida = Serial.parseFloat();
      if (tempIntroducida > 10 && tempIntroducida < 70) {    //Si la temperatura introducida por el monitor serial se encuentra entre 10 y 70 la cambiamos.
        Serial.println(F("Temperatura introducida es de: "));
        Serial.println(tempIntroducida);
        targetTemp = tempIntroducida;
        Serial.println("La nueva temperatura deseada es de: ");
        Serial.println(targetTemp);
      }
    }
  }
}